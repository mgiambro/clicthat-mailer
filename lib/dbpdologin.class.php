<?php


class dbpdologin{

	private static $instance;

	static function loadInstance(){
    	if(!isset(self::$instance)){  
    		$host=settings::getSettings('database-login','hostname');
    		$dbname=settings::getSettings('database-login','dbname');

			try {
			    self::$instance = new PDO("mysql:host=$host;dbname=$dbname", settings::getSettings('database-login','username'), settings::getSettings('database-login','password'));
				self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC); 
			} catch (PDOException $e) {
				log::logError('PDO Connection error '.$e->getMessage());

			    die();
			}
        return self::$instance;
    	}else{
    		debug::add('PDO Info','Instance pre loaded');
    		log::logError('PDO Instance pre loaded');
    	}
	}



    public static function query($sql,$parms=array()){
		self::loadInstance();
		self::debugSql($sql,$parms);
		if(!self::validateSQL($sql,$parms)){return false;}
		$stmt = self::$instance->prepare($sql);
		return self::runStatement($sql,$stmt,$parms,'Prepared completed');

    }

    public static function getQuery($sql,$parms=array()){
 		self::loadInstance();
		self::debugSql($sql,$parms);
		$stmt = self::$instance->prepare($sql);
		if(self::runStatement($sql,$stmt,$parms,'Prepared completed')){
			if($row = $stmt->fetch()) {
	    		return $row;
	  		}else{
	  			debug::add('PDO failed','No rows to return');
	  			return false;
	  		}
		}else{
			debug::add('PDO failed','stmt object not created');
			return false;
		}    
    }

    private static function validateSQL($sql,$parm){
		if(strrpos($sql, '"'>0)){
			debug::add('PDO ERROR','Must use prepared statements 1');
			return false;
		}
		if(strrpos($sql, "'">0)){
			debug::add('PDO ERROR','Must use prepared statements 2');
			return false;
		}
		//now split on the wehere statement and check the rest
		$where=explode('where',strtolower($sql));

		if(array_key_exists(1,$where)){
			$toCheck=$where[1];
		}else{
			//$toCheck=$where[0];
			$toCheck='';
		}
		$eqs=explode('=',$toCheck);
		$first=array_shift($eqs);
		$parmCount=0;
		$valueList='';
		foreach($eqs as $value){
			$valueList.=$value.',';
			//debug::add($value);
			//debug::add(substr($value,0,1));
			if(substr(trim($value),0,1)!=':'){
				debug::add('PDO ERROR','Must use prepared statements 3');
				return false;				
			}
			$parmCount++;
			
		}
		debug::add('Value List ',$valueList);
		if(stripos($sql, "insert into"===0)){
			if($parmCount!=sizeof($parm)){
				debug::add('PDO ERROR','Parameter count mismatch '.sizeof($parm).' passed, expecting '.$parmCount);
				return false;		
			}
		}
		return true;
    }



    public static function dbUpdate($table,$ref,$fields,$fieldref='ref'){
        self::loadInstance();
		$partSql='';
        $sql='update '.$table;
        $sql.=' set ';
		if($table=='_sessionsadmin' || $table=='login' || $table=='promotional_campaign' || $table=='promotional_emails'){
		}else{
			$fields['dateupdated']=date('Y-m-d H:i:s');
			$fields['updatedby']=$_SESSION['login']['ref'];
	        $partSql='dateupdated=:dateupdated, updatedby=:updatedby,';
        }
        foreach($fields as $fieldName=>$value){
            $partSql.=' '.$fieldName.'=:'.$fieldName.',';
            debug::add($fieldName,$value);
        }
        $fields['passedRef']=$ref;
		$partSql=substr($partSql,0,-1);
        $sql.=$partSql.' where '.$fieldref.'=:passedRef limit 1;';
        debug::add('sql',$sql);
        $stmt = self::$instance->prepare($sql);
        self::runStatement($sql,$stmt,$fields,'Update Completed');
    }

    public static function fieldsFromPost($valid,$raw='filter'){
    	//pull valid field values from the post array
    	$retArr=array();
    	foreach($valid as $name){
    		if(clean::post($name,$raw)!=''){
    			$retArr[$name]=clean::post($name,$raw);
    		}
    	}
    	return $retArr;
    }

      public static function dbInsert($table,$fieldList){
        self::loadInstance();
	$excludeFromCreatedFields = array('_sessionsadmin', 'logins', 'promotional_emails', 'promotional_campaign');
	//	if($table!='_sessionsadmin'){
        if(!in_array($table, $excludeFromCreatedFields)){
			$fieldList['datecreated']=date('Y-m-d H:i:s');
			$fieldList['createdby']=$_SESSION['login']['ref'];
        }
		$sql='insert into '.$table.' ';
        $fields='(';
        $values='(';
        foreach($fieldList as $fieldName=>$value){
            debug::add($fieldName,$value);
            $fields.=$fieldName.',';
            $values.=':'.$fieldName.',';
        }
        $fields=substr($fields,0,-1).')';
        $values=substr($values,0,-1).')';
        
        $sql.=$fields.' values '.$values;
        debug::add('PDO sql',$sql);
        $stmt = self::$instance->prepare($sql);
        
   //     echo "SQL:" . $sql;
        self::runStatement($sql,$stmt,$fieldList,'Insert Completed');
		//echo debug::show();
		return self::$instance->lastInsertId();
		
    }

    public static function dbDelete($table,$ref){
    	self::loadInstance();
    	$parm=array('ref'=>$ref);
    	$sql='DELETE FROM '.$table.' WHERE ref=:ref limit 1';
    	$stmt = self::$instance->prepare($sql);
    	self::runStatement($sql,$stmt,$parm,'Delete Completed');

    }

    private static function runStatement($sql,$stmt,$fields,$text){
 		if ($stmt->execute($fields)) {
		  debug::add('PDO'.$text.' Rows affected',$stmt->rowCount());
		  return $stmt;
		}else{
			$error=$stmt->errorInfo();
			debug::add('PDO ERROR',$error[2]);
			debug::add('PDO SQL',$sql);
			//echo 'PDO ERROR'.$error[2];
			return false;
		}   	
    }

    private static function debugSql($sql,$parms){
    	$retVal=$sql;
    	if(sizeof($parms)>0){
	    	foreach($parms as $key=>$value){
	    		$retVal=str_ireplace($key, "'".$value."'", $retVal);
	    	}    		
    	}

    	debug::add('SQL Debug',$retVal);

    }

    private static function shortPath(){
    	$pi=pathinfo(__FILE__);
    	$dn=explode('\\',$pi['dirname']);
    	$f=array_pop($dn);
    	$ret=implode('\\',$dn);
    	return $ret;
    	
    }
	public static function openSession($cmsprefix=''){
		$session = new session(); 
		session_set_save_handler( array($session,'open'), array($session,'close'), array($session,'read'), array($session,'write'), array($session,'destroy'), array($session,'gc') );
		$session->open($cmsprefix.settings::getSettings('project','sessionname'));
		session_start();
	}
	public static function now(){
		return date('Y-m-d H:i:s');
	}


    public static function dbInsertUpdate($table,$ref,$fields,$refField='ref',$insertFunction=''){
        self::loadInstance();
		$parms=array(':ref'=>$ref);
		$sql="select * from $table where $refField=:ref limit 1";
		$stmt=self::query($sql,$parms);
		if ($stmt) {
			$row = $stmt->fetch();
	    	debug::add('dbInsertUpdate',$row[$refField]);
	    	if($row[$refField]>0 || $row[$refField]!=''){
				if($table!='_sessionsadmin'){
					$fields['dateupdated']=date('Y-m-d H:i:s');
					$fields['updatedby']=$_SESSION['login']['ref'];
				}
	    		self::dbUpdate($table,$ref,$fields,$refField);
			}else{
				if($table!='_sessionsadmin'){
					$fields['datecreated']=date('Y-m-d H:i:s');
					$fields['createdby']=$_SESSION['login']['ref'];
				}
				if($insertFunction!=''){
					$fields=call_user_func($insertFunction,$fields);
				}
				self::dbInsert($table,$fields);
			}
		}
	}

}



?>
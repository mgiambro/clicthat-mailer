<?php 

class debug{
	
	private static $debugText;
	private static $pageStartTime;
	private static $errors=false;
	private static $errorCount=0;
	private static $logHandle=false;
	private static $showDebug=false;
	


		static function startTime(){
			self::$pageStartTime=microtime(true);
		}
		
		static function setTimeLog($logText){
			$tt=microtime(true)-self::$pageStartTime;
			self::$debugText.='### page time ### <b>'.$logText.'</b> '.$tt.'<br/>';
		}

		static function add($inBold,$inText=''){
			
			$bt=debug_backtrace();
			$btsub=self::getSubArray($bt,1);
			$bn=basename(self::getSubArray($btsub,'file'));
			if(is_array($inBold)){
				if(sizeof($inBold)>0){
					self::$debugText.=self::oneArray($inBold,$inText);
					

				}else{
					//self::$debugText.=$bn.' <b>'.$bt[1]['class'].'</b>-><b>'.$bt[1]['function'].'</b> (<b>'.$bt[1]['line'].'</b>) '.$inText.' <b>Array Empty</b><br/>';
				}
			}else{
				if(strrpos(strtolower($inBold),'error')===false){
					$inBold='<em>'.$inBold.'</em>';
				}else{
					$inBold='<i>'.$inBold.'</i>';
					self::$errors=true;
					self::$errorCount++;
				}
				self::$debugText.=$bn.' <b>'.self::getSubArray($btsub,'class').'</b>-><b>'.self::getSubArray($btsub,'function').'</b> (<b>'.self::getSubArray($btsub,'line').'</b>) '.$inBold.' <b>'.$inText.'</b><br/>';
				
			}
		}

		private static function getSubArray($inArray,$target){
			$retVal='';
			if(is_array($inArray)){
				if(array_key_exists($target,$inArray)){
				
					$retVal=$inArray[$target];
				}				
			}

			return $retVal;
		}



		

		static function show(){
			
			self::add('lib file',__FILE__);
			$finalTime=microtime(true)-self::$pageStartTime;
			$retVal='<link rel="stylesheet" type="text/css" href="../style/debug.css" media="screen" />';
			$retVal.='<div id="debug-panel"><a name="debugpanel">';
				$retVal.='<div class="debug-sub-panel">';
				if(self::$errors){
					$retVal.='<h5>Debug Output</h5><a href="#debugpanel" class="debug-error-panel">'.self::$errorCount.'</a>';
				}else{
					$retVal.='<h3>Debug Output</h3>';
				}
				
				$retVal.='Page time is '.$finalTime.'<br/>';
				$retVal.=self::$debugText;
				$retVal.='<h3>Page details</h3>';
				$retVal.= "Last modified <b>" . date ("F d Y H:i:s.", getlastmod()).'</b><br/>';
				$retVal.='</div>';
				$retVal.=self::oneArray($_SESSION,'Session data');
				$retVal.=self::oneArray(clean::post(),'POST data');
				$retVal.=self::oneArray(clean::get(),'GET data');
				$retVal.=self::oneArray($_COOKIE,'$_COOKIE');
				$retVal.=self::oneArray(get_included_files(),'Included Files');
				$retVal.=self::oneArray($_SERVER,'$_SERVER');
			$retVal.='</div>';

			return $retVal;
		}
		
		static function oneArray($inArray,$title){
			$retVal='<div class="debug-sub-panel">';
				$retVal.='<h3>'.$title.'</h3>';
				$retVal.=self::walker($inArray);
			$retVal.='</div>';
			return $retVal;
		}
		
		static function walker($inArray,$depth=0){
			$depth++;
			$retVal='';
			if(sizeOf($inArray)>0){
				foreach($inArray as $key=>$value){
					if(is_array($value)){
						$retVal.='<div class="debug-sub-array">';
						$retVal.= '<h4>Array [<b>'.$key.']</b></h4>';
						$retVal.=self::walker($value,$depth);
						$retVal.='</div>';
					}else{
						if(!strrpos(strtolower($key),'error'===false)){
							$retVal.= '<em>'.$key.'</em> <b>'.$value.'</b><br/>';
						}else{
							$retVal.= $key.' <b>'.$value.'</b><br/>';
						}
						
					}				
				}
			}else{
				$retVal.='Empty Array';
			}
			return $retVal;
		}



		



	static function logErrors($showDebug=false){
		self::$showDebug=$showDebug;
		set_error_handler(array('debug','localerror'),E_ALL);
		register_shutdown_function(array('debug','shutdown'));
	}




    static function localerror($errNo,$errString){
        self::log('ERROR '.$errNo.':'.$errString);
        self::add('PHP ERROR '.$errNo,$errString);
        return false;
    }


		static function clearLog(){
			
			$logHandle=fopen(settings::getSettings('project','debugpath'), 'w+');
			fclose($logHandle);
		}

		static function log($logtext){		
			$txt=date('H:i:s');
			if(is_array($logtext)){
				$txt.='Array Found ';
				$txt.=self::walker($logtext);
			}else{
				$txt.=$logtext;
			}
			$logHandle=fopen(settings::getSettings('project','debugpath'), 'a');
			fwrite($logHandle, $txt.'</br>');
			fclose($logHandle);

		}


	static function shutdown() {
	    $isError = false;
	    $errorType='Unknown';

	    if ($error = error_get_last()){
	    switch($error['type']){
	        case E_ERROR:
	        	$errorType='E_ERROR';
	        case E_CORE_ERROR:
	        	$errorType='E_CORE_ERROR';
	        case E_COMPILE_ERROR:
	        	$errorType='E_COMPILE_ERROR';
	        case E_WARNING:
	        	$errorType='E_WARNING';
	        case E_NOTICE:
	        	$errorType='E_NOTICE';
	        case E_USER_ERROR:
	        	$errorType='E_USER_ERROR';
	            $isError = true;
	            break;
	        }
	    }

	    $errorType.=' '.$error['type'];

	    if ($isError){
	    	debug::log('SHUTDOWN ERROR '.$errorType.' '.$error['message'].' line '.$error['line']);
	    	debug::add('SHUTDOWN ERROR '.$errorType,$error['message'].' line '.$error['line']);

	    	
	    	//echo debug::show();
	    }
	    if(self::$showDebug){
		    debug::log('### START OF STORED DEBUG OUTPUT ###');
		    debug::log(debug::show());
		    debug::log('### END OF STORED DEBUG OUTPUT ###');	    	
	    }

	}

}
?>
<?php



interface iporticorendpoint {

    public function getVersion();

    public function getTime();

    public function getTempCred();

    public function getProjects();

    public function getStatus();

    public function createProtectedItemGenerate($itemName, $length);

    public function createProtectedItem($itemName, $item);

    public function clearProtectedItem($itemName);

    public function deleteProtectedItem($itemName);

    public function getProtectedItem($itemName);

    public function getCombinedKey($uniqueKey);
}

abstract class http_method {

    const GET = 'GET';
    const DELETE = 'DELETE';
    const PUT = 'PUT';
    const POST = 'POST';

}

abstract class on_error {

    const FAIL = 'FAIL';
    const EXPECT = 'EXPECT';
    const IGNORE = 'IGNORE';

}

class porticorexception extends Exception {

    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 0, Exception $previous = null) {
        // some code
        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
    }

    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }

}

class porticorendpoint implements iporticorendpoint {

    private $overallKey = "";
    private $tempCred = "";

    /**
     * Constructor
     *
     * @param
     *        	keyName - name of overall key
     */
    function porticorendpoint() {
        $parameters = func_get_args();
        $numargs = func_num_args();
        // Set tempCred

        $this->setTempCred();
        // Set overallKey member variable
        if ($numargs >= 1) {
            $this->setOverallKey($parameters [0]);
        }
    }

    private function setTempCred() {
        //	 $time_pre_setTempCred = microtime(true);
        $this->tempCred = $this->getTempCred();
        //	 $time_post_setTempCred = microtime(true);
        //	$setTempCred_execute_time = $time_post_setTempCred - $time_pre_setTempCred;
        //	 echo "TEMP CRED EXEC TIME: " . $setTempCred_execute_time;
        //	 echo "<br/><br/>";
    }

    function getCombinedKey($uniqueKey) {
        /*
          echo "MASTER KEY: " . $this->overallKey;
          echo "<br/><br/>";
          echo "UNIQUE KEY: " . $uniqueKey;
         */
        return $uniqueKey . $this->overallKey;
    }

    /**
     *
     * @param
     *        	keyName - overall key set for all data items
     */
    private function setOverallKey($keyName) {
        $porticorKey = null;
        try {
            $porticorKey = $this->getProtectedItem($keyName);
        } catch (porticorexception $pe) {
            echo $pe;
        }
        $this->overallKey = $porticorKey;
    }

    /**
     * Checks porticor to see if a given keys exists
     *
     * @param
     *        	keyName
     */
    function keyExists($keyName) {
        try {
            $ret = $this->getProtectedItem($keyName, $this->tempCred);
        } catch (porticorexception $pe) {
            echo $pe->getMessage();
            return false;
        }
        return true;
    }

    /**
     * Clear a protected item, whether it exists or not
     *
     * @param
     *        	itemName
     */
    function clearProtectedItem($itemName) {
        $params = $this->buildUriParams(array(
            'api_cred' => $this->tempCred
                ));
        $res = $this->request2("/api/protected_items/" . $itemName, $params, http_method::DELETE, on_error::IGNORE);
    }

    /**
     * Create a new, protected item
     *
     * @param
     *        	itemName
     * @param
     *        	item
     */
    function createProtectedItem($itemName, $item) {
        $params = $this->buildUriParams(array(
            'item' => $item,
            'api_cred' => $this->tempCred
                ));

        $res = $this->request2("/api/protected_items/" . $itemName, $params, http_method::PUT, on_error::FAIL);
        /*
         * while ( list ( $key, $val ) = each ( $res ) ) echo $val;
         */
        // var_dump($res) ;
        return $res;
    }

    /**
     * Create a new, random protected item
     *
     * @param
     *        	itemName
     * @param
     *        	length
     */
    function createProtectedItemGenerate($itemName, $length) {
        $params = $this->buildUriParams(array(
            'generate' => $length,
            'api_cred' => $this->tempCred
                ));

        $res = $this->request2("/api/protected_items/" . $itemName, $params, http_method::PUT, on_error::FAIL);
        // while (list ($key, $val) = each ($res) ) echo $val;
    }

    /**
     * Read a protected item.
     *
     * @param
     *        	itemName
     * @return
     *
     *
     *
     *
     *
     *
     */
    function getProtectedItem($itemName) {
        $params = $this->buildUriParams(array(
            'api_cred' => $this->tempCred
                ));

        $res = $this->request1("/api/protected_items/" . $itemName, $params);

        $item = $res ['item'];

        if ($item == null) {
            $this->createProtectedItemGenerate($itemName, 32);
        }

        $metadata = $res ['metadata'];

        return $item;
    }

    /**
     * Delete a protected item.
     * Fails if the item does not exist.
     *
     * @param
     *        	itemName
     * @param
     *        	tempCred
     */
    function deleteProtectedItem($itemName) {
        $params = $this->buildUriParams(array(
            'api_cred' => $this->tempCred
                ));
        $res = $this->request2("/api/protected_items/" . $itemName, $params, http_method::DELETE, on_error::FAIL);
    }

    /**
     * Get the API version of the appliance.
     *
     * @return Version as a string
     */
    function getVersion() {
        $res = $this->request("/api/version");
        $version = $res ['version'];
        // log::logInfo($version);
        return $version;
    }

    /**
     * Get Unix time, as reported by the appliance.
     *
     * @return Unix time, seconds since the epoch
     */
    function getTime() {
        $res = $this->request("/api/creds/get_time");
        $time = $res ['time'];
        // log::logInfo(strval($time));
        return $time;
    }

    /**
     * Get a temporary credential for this session.
     *
     * @param
     *        	apiKeyId
     *        	Key ID
     * @param
     *        	apiSecretKey
     *        	Secret key (the actual secret for v1 keys, and the file name
     *        	for v2 keys)
     * @param
     *        	time
     *        	Unix time (must be "close enough" to the time on the
     *        	appliance)
     * @return The credential
     */

    /**
     * Get a list of projects for the current organization
     *
     * @param
     *        	tempCred
     *        	Session temporary credential.
     * @return An array of projects.
     */
    function getProjects() {
        $params = $this->buildUriParams(array(
            'api_cred' => $this->tempCred
                ));
        $projects = $this->request1("/api/projects", $params);

        return $projects;
    }

    /**
     * Get status for appliance
     *
     * @return An array of status information.
     */
    function getStatus() {
        $params = $this->buildUriParams(array(
            'api_cred' => $this->tempCred
                ));
        $status = $this->request("/api/status", $params);

        return $status;
    }
    
    /**
     * Get a temporary credential for this session.
     *
     * @param
     *        	apiKeyId
     *        	Key ID
     * @param
     *        	apiSecretKey
     *        	Secret key (the actual secret for v1 keys, and the file name
     *        	for v2 keys)
     * @param
     *        	time
     *        	Unix time (must be "close enough" to the time on the
     *        	appliance)
     * @return The credential
     */
    function getTempCred1($apiKeyId, $apiSecretKey, $time) {
        $nonce = $this->generateNonce(16);
        $signature = $this->signCredRequest($apiKeyId, $apiSecretKey, $nonce, strval($time));

        $uriParams = $this->buildUriParams(array(
            'api_key_id' => $apiKeyId,
            'time' => strval($time),
            'nonce' => $nonce,
            'api_signature' => $signature
                ));

        $res = $this->request1("/api/creds/get_temporary_credential", $uriParams);

        $credential = $res ['credential'];

        return $credential;
    }

    /**
     * Get a temporary credential, using the currently configured parameters.
     *
     * @return Temporary credential
     */
    function getTempCred() {
        $apiKeyId = settings::getSettings('porticor', 'api_key_id');
        $apiSecretKey = settings::getSettings('porticor', 'api_secret_key');
        $time = $this->getTime();
        return $this->getTempCred1($apiKeyId, $apiSecretKey, $time);
    }

    // private members
    /**
     * Sign a request to fetch the temporary credential, with either a version 1
     * or version 2 key.
     *
     * @param
     *        	apiKeyId
     *        	Key ID
     * @param
     *        	apiSecretKey
     *        	Secret key (the actual secret for v1 keys, and the file name
     *        	for v2 keys)
     * @param
     *        	nonce
     *        	A fresh random nonce
     * @param
     *        	time
     *        	Unix time (must be "close enough" to the time on the
     *        	appliance)
     * @return The signed request
     */
    private function signCredRequest($apiKeyId, $apiSecretKey, $nonce, $time) {
        $strToSign = "get_temporary_credential?api_key_id=" . $apiKeyId . "&nonce=" . $nonce . "&time=" . strval($time);

        // log::logInfo("strToSign: " . $strToSign);
        // Fetch private key from file and ready it.
        $private_key = $this->readPrivateKey($apiSecretKey);

        // Create signature
        $binary_signature = "";
        $algo = OPENSSL_ALGO_SHA1;
        openssl_sign($strToSign, $binary_signature, $private_key, $algo);

        $encoded_signature = "rsa-sha1:" . base64_encode($binary_signature);

        return $encoded_signature;
    }

    /**
     * Utility function: build name/value pairs from a varargs list.
     *
     * @param
     *        	args
     * @return
     *
     *
     *
     *
     *
     *
     *
     */
    private function buildUriParams($params) {
        return http_build_query($params, '', '&');
    }

    /**
     * Read an RSA private key from a PEM file
     *
     * @param
     *        	apiSecretKeyFileName
     *        	File name
     * @return The validated RSA private key
     */
    private function readPrivateKey($apiSecretKeyFileName) {
        $private_key = null;

        try {
            $private_key = openssl_pkey_get_private($this->fetchContents($apiSecretKeyFileName));
        } catch (Exception $e) {
            log::logError("Failed to read private key: ", $e->getMessage(), "\n");
            echo "Caught exception: ", $e->getMessage(), "\n";
        }
        return $private_key;
    }

    private function fetchContents($file) {
        $contents = file_get_contents($file);
        if (false === $contents) {
            throw new Exception("Load Failed");
        } else {
            return $contents;
        }
    }

    private function generateNonce($length) {
        $nonceChars = "0123456789abcdef";
        $result = "";
        while (strlen($result) < $length) {
            $pos = mt_rand(1, 16);
            $result .= substr($nonceChars, $pos, 1);
        }
        return $result;
    }

    /**
     * Make a request with no parameters
     *
     * @param
     *        	req
     * @return
     *
     *
     *
     *
     *
     */
    private function request($req) {
        return $this->request1($req, null);
    }

    /**
     * Make a regular Get request
     *
     * @param
     *        	req
     * @param
     *        	params
     * @return
     *
     *
     *
     *
     *
     */
    private function request1($req, $params) {
        return $this->request2($req, $params, http_method::GET, true, false, true);
    }

    /**
     * Make a request to the appliance.
     *
     * @param
     *        	req
     *        	Request base URL
     * @param
     *        	params
     *        	Request parameters
     * @param
     *        	method
     *        	HTTP method
     * @param
     *        	onError
     *        	Behavior if the appliance returns an error
     * @return The JSON result of the request
     */
    private function request2($req, $params, $httpMethod, $onError) {
        try {
            $ret = $this->request_raw($req, $params, $httpMethod, true, false, true, false);       
        if($ret==false){
            // Run on failover appliance
            $ret = $this->request_raw($req, $params, $httpMethod, true, false, true, true);
        }
        } catch (Exception $e) {
            // log::logError('Could not parse JSON response: ', $e->getMessage(), "\n");
            echo 'Caught exception: ', $e->getMessage(), "\n";
            return null;
        }

        $errorCode = $ret ['error_code'];
        $errorMsg = $ret ['error'];

        switch ($onError) {
            case on_error::FAIL :
                if ($errorCode != "") {
                    // log::logError('API returned error: '.errorCode.": ".errorMsg, $e->getMessage(), "\n");
                }
                break;
            case on_error::EXPECT :
                if ($errorCode == "") {
                    // log::logError('API did not return an expected error, "\n");
                }
            case on_error::IGNORE :
            // log::logError('Ignoring an expected API error: '.errorCode.": ".errorMsg, $e->getMessage(), "\n");
        }

        return $ret;
    }

    function ping($host, $port = 80, $timeout = 6) {
        $fsock = fsockopen($host, $port, $errno, $errstr, $timeout);
        if (!$fsock) {
            return FALSE;
        } else {
            return TRUE;
        }
    }
    
    function buildRequestURL($req, $params, $failover){
        
        if($failover==true){
           $appliance = settings::getSettings('porticor', 'appliance_failover'); 
        }else{
            $appliance = settings::getSettings('porticor', 'appliance');
        }
           

        if ($params != null && $params [0] != "") {
            $json_url = "https://" . $appliance . $req . "?" . $params;
        } else {
            $json_url = "https://" . $appliance . $req;
        }
     //  $res = exec_curl($json_url);
//        echo $json_url;
        return $json_url;
    }

    /**
     * Make a request to the appliance, return the response as a raw string.
     *
     * @param
     *        	req
     *        	The URL
     * @param
     *        	params
     *        	Request parameters
     * @param
     *        	method
     *        	HTTP method to be used
     * @return The response
     */
    private function request_raw($req, $params, $method, $return_array = true, $print_array = false, $curl, $failover=false) {
        $jsonString = '';
   //     $data = array();

        $json_url = $this->buildRequestURL($req, $params, $failover);
      //  echo "<br/><br/>" . $json_url;

        if (!$curl) {
            /*
             * if !$curl, use "file_get_contents" method to get JSON encoded string
             */
            $jsonString = file_get_contents($json_url);
        } else {
            /*
             * if $curl, use "curl" method to get JSON encoded string
             */

            // Initializing curl
            $ch = curl_init($json_url);
            
            // Check for errors
            if (curl_error($ch) != '') {
                // ADD LOGGING HERE
                echo "error: " . curl_error($ch);
            }

            // Configuring curl options
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_SSL_VERIFYHOST => 2,
                CURLOPT_CUSTOMREQUEST => $method,
                CURLOPT_HTTPHEADER => array(
                    'Content-type: application/json'
                )
            );

            // Setting curl options
            curl_setopt_array($ch, $options);

            // Getting JSON encoded string
            $jsonString = curl_exec($ch);

            // If $jsonString returns false, run request on failover appliance
            if($jsonString==false){
                return false;
            }
        }

        // echo "jsonString: ". $jsonString . "<br/>";
        // convert the JSON encoded string into a PHP variable(array)
        // if($return_array)
        $data = json_decode($jsonString, $return_array);

        // $print_array == true, print the array
        if ($print_array) {
            echo '<pre>';
            print_r($data);
            echo '</pre>';
        }

        return $data;
    }

    private function get_json_data($json_url, $return_array = true, $print_array = false, $curl = true) {
        $jsonString = '';
        $data = array();
        if (!$curl) {
            /*
             * if !$curl, use "file_get_contents" method to get JSON encoded string
             */
            $jsonString = file_get_contents($json_url);
        } else {
            /*
             * if $curl, use "curl" method to get JSON encoded string
             */

            // Initializing curl
            $ch = curl_init($json_url);

            // Configuring curl options
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HTTPHEADER => array(
                    'Content-type: application/json'
                )
            );

            // Setting curl options
            curl_setopt_array($ch, $options);

            // Getting JSON encoded string
            $jsonString = curl_exec($ch);
        }

        // convert the JSON encoded string into a PHP variable(array)
        // if($return_array)
        $data = json_decode($jsonString, $return_array);

        // $print_array == true, print the array
        if ($print_array) {
            echo '<pre>';
            print_r($data);
            echo '</pre>';
        }

        return $data;
    }

    // The PHP hack for method overloading
    public function __call($method_name, $arguments) {
        $methodArray = array(
            'getTempCred',
            'request'
        );

        if (in_array($method_name, $methodArray) === false) {
            die("\n Method does not exist");
        }

        switch ($method_name) {
            case 'getTempCred' :
                if (count($arguments) === 3) {
                    $this->getTempCred1($arguments [0], $arguments [1], $arguments [2]);
                } elseif (count($arguments) === 0) {
                    $this->getTempCred();
                } else {
                    echo "\n unknown method";
                    return false;
                }
            case 'request' :
                if (count($arguments) === 4) {
                    $this->request2($arguments [0], $arguments [1], $arguments [2], $arguments [3]);
                } elseif (count($arguments) === 2) {
                    $this->request1($arguments [0], $arguments [1]);
                } else if (count($arguments) === 1) {
                    $this->request($arguments [0]);
                } else {
                    echo "\n unknown method";
                    return false;
                }
        }
    }

}

<?php

use Aws\Common\Credentials\Credentials;
use Aws\Ses\SesClient;

require_once '/var/ct/lib/log.class.php';
require_once '/var/ct/lib/dbpdologin.class.php';
require_once '/var/ct/lib/settings.class.php';
require_once '/var/ct/lib/dbpdo.class.php';

/**
 * Description of Statistics
 *
 * @author mgiambro
 */
class Statistics {

    private $sesClient = null;
    private $porticor;
    private $sesStatistics;
    private $deliveryAttempts;
    private $bounces;
    private $complaints;
    private $rejects;

    const DEL_ATTEMPTS = "DeliveryAttempts";
    const BOUNCES = "Bounces";
    const COMPLAINTS = "Complaints";
    const REJECTS = "Rejects";
    const OPEN_RATE_AVG = "15.56";
    const CLICK_THRU_AVG = "8.67";
    const OPT_OUT_AVG = "0.91";

    function __construct($setup = true) {
        if ($setup == true) {
            $credentials = new Credentials(settings::getSettings('amazons3', 'accesskey'), settings::getSettings('amazons3', 'Secretkey'));
            $this->sesClient = SesClient::factory(array(
                        'credentials' => $credentials,
                        'region' => 'eu-west-1'));
//     $this->porticor = new porticorendpoint(settings::getSettings('porticor', 'current_master_key_name'));

            $this->sesStatistics = $this->getSendStatistics();
        }
    }

    public function getSESStat($startDatetime, $statName, $period) {
        // The number of delivery attempts within 2 hours of the start of the campaign.
        $startTime = new DateTime($startDatetime);
        $startTime->sub(new DateInterval('PT15M'));
        $endTime = new DateTime($startDatetime);
        $endTime->add(new DateInterval($period));
        $sesSendDataPoints = $this->sesStatistics['SendDataPoints'];
        $deliveryAttempts = 0;

        foreach ($sesSendDataPoints as $interval) {
            // Only include intervals that have occurred since $startDatetime
            $intervalTS = new DateTime($interval['Timestamp']);
            if ($intervalTS >= $startTime && $intervalTS <= $endTime) {
                echo "Interval: " . $interval['Timestamp'] . " Delivery Attempts: " . $interval['DeliveryAttempts'];
                echo "<br/>";
                $deliveryAttempts += $interval[$statName];
            }
        }
        return $deliveryAttempts;
    }

    public function updateCampaignTable($startDatetime, $campaignCode) {

        // Only update if now() is between window start and window end
        $windowStart = new DateTime(date($startDatetime));
        $windowEnd = new DateTime(date($startDatetime));
        // End window + 6 days - 12 hours to avoid overlap
        $windowEnd->add(new DateInterval('P6DPT12H'));
        $now = new DateTime(date('Y-m-d H:i:s'));

        $recipients = $this->getRecipients($campaignCode);
        $opened = $this->getOpened($campaignCode);
        $clicked = $this->getClicked($campaignCode);
        // Only update SES stats for 1 week following the mailout
        if ($now >= $windowStart && $now <= $windowEnd) {
            //        echo "UPDATE";
            //       echo var_dump($windowEnd);
            $fields = array(
                'recipients' => $recipients,
                'opened' => $opened,
                'open_rate' => $this->getAverage($recipients, $opened),
                'delivery_attempts' => $this->getSESStat($startDatetime, Statistics::DEL_ATTEMPTS, 'PT4H'),
                'rejected' => $this->getSESStat($startDatetime, Statistics::REJECTS, 'PT4H'),
                'bounced' => $this->getSESStat($startDatetime, Statistics::BOUNCES, 'PT4H'),
                'complaints' => $this->getSESStat($startDatetime, Statistics::COMPLAINTS, 'P7D'),
                'unsubscribed' => $this->getUnsubscribed($startDatetime),
                'last_opened' => $this->getLastOpened($campaignCode),
                'last_updated' => date('Y-m-d H:i:s'),
                'times_opened' => $this->getTimesOpened($campaignCode),
                'total_clicks' => $this->getTimesClicked($campaignCode),
                'last_clicked' => $this->getLastClicked($campaignCode),
                'clicked' => $clicked,
                'clickthrough_rate' => $this->getAverage($opened, $clicked),
            );
            //      echo var_dump($fields); 
        } else {
            $fields = array(
                'recipients' => $recipients,
                'opened' => $opened,
                'open_rate' => $this->getAverage($recipients, $opened),
                'last_opened' => $this->getLastOpened($campaignCode),
                'last_updated' => date('Y-m-d H:i:s'),
                'times_opened' => $this->getTimesOpened($campaignCode),
                'total_clicks' => $this->getTimesClicked($campaignCode),
                'last_clicked' => $this->getLastClicked($campaignCode),
                'clicked' => $clicked,
                'clickthrough_rate' => $this->getAverage($opened, $clicked),
            );
        }
        dbpdologin::dbUpdate('promotional_campaign', $campaignCode, $fields, 'campaign_code');
    }

    public function getDateSent($campaignCode) {
        $sql = 'SELECT timedate_sent from promotional_campaign WHERE campaign_code = :campaign_code limit 1';

        $params = array('campaign_code' => $campaignCode);
        $stmt = dbpdologin::query($sql, $params);

        if ($stmt) {
            $lastOpenedArr = $stmt->fetch(PDO::FETCH_NUM);
            return $lastOpenedArr[0];
        } else {
            return false;
        }
    }

    public function getRecipients($campaignCode) {
        $sql = 'SELECT  recipients from promotional_campaign WHERE campaign_code = :campaign_code';
        $params = array('campaign_code' => $campaignCode);
        $stmt = dbpdologin::query($sql, $params);

        if ($stmt) {
            return $stmt->fetchColumn();
        } else {
            return false;
        }
    }

    public function getOpened($campaignCode) {
        $sql = 'SELECT * from promotional_emails WHERE opened = :opened AND campaign_code = :campaign_code';
        $params = array('opened' => 'y', 'campaign_code' => $campaignCode);
        $stmt = dbpdologin::query($sql, $params);

        if ($stmt) {
            return $stmt->rowCount();
        } else {
            return false;
        }
    }

    public function getTimesOpened($campaignCode) {
        $sql = 'SELECT SUM(`times_opened`) from promotional_emails WHERE campaign_code = :campaign_code';
        $params = array('campaign_code' => $campaignCode);
        $stmt = dbpdologin::query($sql, $params);

        if ($stmt) {
            $timesOpenedArr = $stmt->fetch(PDO::FETCH_NUM);
            return $timesOpenedArr[0];
        } else {
            return false;
        }
    }

    public function getClicked($campaignCode) {
        $sql = 'SELECT * from promotional_emails WHERE times_clicked > :zero AND campaign_code = :campaign_code';
        $params = array('zero' => 0, 'campaign_code' => $campaignCode);
        $stmt = dbpdologin::query($sql, $params);

        if ($stmt) {
            return $stmt->rowCount();
        } else {
            return false;
        }
    }

    public function getTimesClicked($campaignCode) {
        $sql = 'SELECT SUM(`times_clicked`) from promotional_emails WHERE campaign_code = :campaign_code';
        $params = array('campaign_code' => $campaignCode);
        $stmt = dbpdologin::query($sql, $params);

        if ($stmt) {
            $timesClickedArr = $stmt->fetch(PDO::FETCH_NUM);
            return $timesClickedArr[0];
        } else {
            return false;
        }
    }

    public function getLastOpened($campaignCode) {
        $sql = 'SELECT MAX(`timedate_opened`) from promotional_emails WHERE campaign_code = :campaign_code limit 1';

        $params = array('campaign_code' => $campaignCode);
        $stmt = dbpdologin::query($sql, $params);

        if ($stmt) {
            $lastOpenedArr = $stmt->fetch(PDO::FETCH_NUM);
            return $lastOpenedArr[0];
        } else {
            return false;
        }
    }

    public function getLastClicked($campaignCode) {
        $sql = 'SELECT MAX(`last_clicked`) from promotional_emails WHERE campaign_code = :campaign_code limit 1';

        $params = array('campaign_code' => $campaignCode);
        $stmt = dbpdologin::query($sql, $params);

        if ($stmt) {
            $lastClickedArr = $stmt->fetch(PDO::FETCH_NUM);
            return $lastClickedArr[0];
        } else {
            return false;
        }
    }

    public function getAverage($of, $value) {
        $percentage = 0;
        if ($value > 0 && $of > 0) {
            $percentage = ($value / $of) * 100;
        }
        return round($percentage, 2);
    }

    public function getUnsubscribed($dateSent) {
        $sql = 'SELECT COUNT(`md5`) from customer WHERE marketing = :out AND dateupdated >= :date_sent';
        $params = array('out' => 'Out', 'date_sent' => $dateSent);
        $stmt = dbpdo::query($sql, $params);

        if ($stmt) {
            $lastOpenedArr = $stmt->fetch(PDO::FETCH_NUM);
            return $lastOpenedArr[0];
        } else {
            return false;
        }
    }

    public function getSendStatistics() {
        $params = array();

        $result = $this->sesClient->getSendStatistics($params);
        return $result;
    }

}

class CampaignController {

    public function getCustomerMD5($ref) {
        $sql = "SELECT md5 from customer WHERE ref = :ref LIMIT 1";

        $parms = array(':ref' => $ref);
        $stmt = dbpdo::query($sql, $parms);
        if ($stmt) {
            $md5 = $stmt->fetchColumn();
        } else {
            return false;
        }
        return $md5;
    }

    private function getUnsubscribeRefs($startDatetime) {
        // Only return if now() is between window start and window end
        $windowStart = $startDatetime;
        $windowEnd = clone $windowStart;
        // End window + 1 week - 1 hour to avoid overlap
        $windowEnd->add(new DateInterval('P6DPT12H'));

        $sql = 'select ref from customer WHERE marketing=:marketing and dateupdated > :window_start and dateupdated < :window_end';

        /*
          $parms = array('marketing' => 'Out', 'window_start' => $windowStart->format('Y-m-d H:i:s'),
          'window_end' => $windowEnd->format('Y-m-d H:i:s'));

         */

        $parms = array('marketing' => 'Out', 'window_start' => '2014-06-09 12:03:53',
            'window_end' => '2014-12-15 15:16:45');

        $stmt = dbpdo::query($sql, $parms);
        if ($stmt) {
            $refs = array();
            while ($entry = $stmt->fetch()) {
                $refs[] = $entry['ref'];
            }
            return $refs;
        } else {
            return false;
        }
    }

    public function getAllCampaigns() {
        $rows = array();

        $sql = 'select * from promotional_campaign';
        $stmt = dbpdologin::query($sql);

        if ($stmt) {
            $rows = $stmt->fetchAll();
        }
        return $rows;
    }

    public function getCampaign($code) {
        $row = array();
        $parms = array(':campaign_code' => $code);
        $sql = 'select * from promotional_campaign where campaign_code=:campaign_code limit 1';
        $stmt = dbpdologin::query($sql, $parms);
        if ($stmt) {
            $row = $stmt->fetch(PDO::FETCH_OBJ);
        }
        return $row;
    }

}

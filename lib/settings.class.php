<?php 

class settings{
	
	private static $settingsArray=array();

	private static function load_settings(){
		//if(include '../../lib/_settings.php'){
		//}else if(include '../lib/_settings.php'){
		//}else{
		//	include '../../../lib/_settings.php';
		//}
		$pathinfo=pathinfo(__FILE__);
		$path=$pathinfo['dirname'].'/_settings.php';
		include $path;



		self::$settingsArray=$global_settings;
        //        echo var_dump(self::$settingsArray);
	}
	
	static function getSettings($section='',$item=''){
		if(sizeof(self::$settingsArray)==0){
			self::load_settings();
		}
		if($item==''){
			return self::$settingsArray[$section];
		}else{
			return self::$settingsArray[$section][$item];
		}

	}	

	public static function openSession($cmsprefix=''){
		session_name($cmsprefix.settings::getSettings('project','sessionname'));
		debug::add('session id',session_id());
		session_start();
	}

	static function openSessionFailed($cmsprefix=''){
		$session=$cmsprefix.self::$settingsArray['project']['sessionname'];
		debug::add('Session Starting',$session);
		session_name($session);
		@session_start();
		debug::add('Session Id',session_id());
		debug::add($_SESSION,'session data');
		debug::add('Session Started',$session);
	}

	public static function session($inArr){
		$cArr=$_SESSION;
		foreach($inArr as $key){
			if(array_key_exists($key, $cArr)){
				//print_r($cArr);
				$cArr=$cArr[$key];
				
			}else{
				debug::add('WARNING','Missing session key '.$key);
				return '';
			}
		}
		debug::add($cArr,'c array');
		return $cArr;
	}

}


?>
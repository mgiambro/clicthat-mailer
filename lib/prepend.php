<?php

include ('_autoload.php');
$session = new session();
session_set_save_handler(array($session, 'open'), array($session, 'close'), array($session, 'read'), array($session, 'write'), array($session, 'destroy'), array($session, 'gc'));
$session->open(settings::getSettings('project', 'sessionname'));
session_start();

if (!loggingIn()) {
    /*
     * If the period between the login page opening and the user clicking the login button
     * is greater than the session timeout period, the following code will clear the session
     * and prevent the user logging in, despite entering the correct credentials.
     * The validatingLogin check prevents the session being cleared in this usecase. 
     */
    if (isset($_SESSION['LAST_ACTIVITY']) && (time() - $_SESSION['LAST_ACTIVITY'] > 1800)) {
        echo "DESTROYING SESSION!!";
        // last request was more than 30 minutes ago
        session_unset();     // unset $_SESSION variable for the run-time 
        session_destroy();   // destroy session data in storage   
    }
}

$_SESSION['LAST_ACTIVITY'] = time(); // update last activity time stamp

clean::setprepend();
clean::cleanInput();
unset($_POST);
unset($_GET);
if (strpos(getcwd(), 'login') > 0) {
    debug::add('### prepend ###', 'Check for login');
} elseif (strpos(getcwd(), 'cron') > 0) {
    debug::add('### prepend ###', 'Check for login');
} else {
    debug::add('### prepend ###', 'Check for login');
    if (login_login::checkLoggedIn()) {
        debug::add('### logged in ###');
    } else {
        debug::add('*** not logged in ***');
        header('Location: ../login/login.php');
        //echo debug::show();
    }
}

function loggingIn() {
    $requestURI = $_SERVER['REQUEST_URI'];
    $validate = '_validate.php';
    $login = 'login.php';
 
    $validatepage = trim(substr($requestURI, -13));
    $loginpage = trim(substr($requestURI, -9));

    $validatingLogin = strcmp($validatepage, $validate);
    $onLoginPage = strcmp($loginpage, $login);
  
    if ($validatingLogin == 0 || $onLoginPage == 0) {
        return true;
    } else {
        return false;
    }
}

?>

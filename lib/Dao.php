<?php

namespace SesMailer\Lib;

class Dao {

    private static $instance;

    function __construct() {
        
    }

    public function query($sql, $parms = array()) {
        self::loadInstance();
        //	self::debugSql($sql,$parms);
        if (!self::validateSQL($sql, $parms)) {
            return false;
        }
        $stmt = self::$instance->prepare($sql);

        return self::runStatement($sql, $stmt, $parms, 'Prepared completed');
    }

    public function getQuery($sql, $parms = array()) {

        return 'true';
    }

    private function validateSQL($sql, $parm) {
        if (strrpos($sql, '"' > 0)) {
//			debug::add('PDO ERROR','Must use prepared statements 1');
            return false;
        }
        if (strrpos($sql, "'" > 0)) {
//			debug::add('PDO ERROR','Must use prepared statements 2');
            return false;
        }
        //now split on the wehere statement and check the rest
        $where = explode('where', strtolower($sql));

        if (array_key_exists(1, $where)) {
            $toCheck = $where[1];
        } else {
            //$toCheck=$where[0];
            $toCheck = '';
        }
        $eqs = explode('=', $toCheck);
        $first = array_shift($eqs);
        $parmCount = 0;
        $valueList = '';
        foreach ($eqs as $value) {
            $valueList.=$value . ',';
            //debug::add($value);
            //debug::add(substr($value,0,1));
            if (substr(trim($value), 0, 1) != ':') {
//				debug::add('PDO ERROR','Must use prepared statements 3');
                return false;
            }
            $parmCount++;
        }
//		debug::add('Value List ',$valueList);
        if (stripos($sql, "insert into" === 0)) {
            if ($parmCount != sizeof($parm)) {
//				debug::add('PDO ERROR','Parameter count mismatch '.sizeof($parm).' passed, expecting '.$parmCount);
                return false;
            }
        }
        return true;
    }

    public function dbUpdate($table, $id, $fields, $fieldref = 'id') {
    
        foreach ($fields as $fieldName => $value) {
            $partSql.=' ' . $fieldName . '=:' . $fieldName . ',';
        }
        $fields['passedRef'] = $id;
        $partSql = substr($partSql, 0, -1);
        $sql.=$partSql . ' where ' . $fieldref . '=:passedRef limit 1;';
        $stmt = self::$instance->prepare($sql);
        self::runStatement($sql, $stmt, $fields, 'Update Completed');
        self::loadInstance();
        $partSql = '';
        $sql = 'update ' . $table;
        $sql.=' set ';
    
        foreach ($fields as $fieldName => $value) {
            $partSql.=' ' . $fieldName . '=:' . $fieldName . ',';
        }
        $fields['passedRef'] = $ref;
        $partSql = substr($partSql, 0, -1);
        $sql.=$partSql . ' where ' . $fieldref . '=:passedRef limit 1;';
        debug::add('sql', $sql);
        $stmt = self::$instance->prepare($sql);
        self::runStatement($sql, $stmt, $fields, 'Update Completed');
    }

    public function dbInsert($table, $fieldList) {
        self::loadInstance();

        $sql = 'insert into ' . $table . ' ';
        $fields = '(';
        $values = '(';
        foreach ($fieldList as $fieldName => $value) {

            $fields.=$fieldName . ',';
            $values.=':' . $fieldName . ',';
        }
        $fields = substr($fields, 0, -1) . ')';
        $values = substr($values, 0, -1) . ')';

        $sql.=$fields . ' values ' . $values;
        $stmt = self::$instance->prepare($sql);

        //  echo "SQL:" . $sql;
        self::runStatement($sql, $stmt, $fieldList, 'Insert Completed');

        return self::$instance->lastInsertId();
    }

    public function dbDelete($table, $ref) {
        return 'true';
    }

    private function now() {
        return 'true';
    }

    private function runStatement($sql, $stmt, $fields, $text) {
        echo var_dump($sql);
        if ($stmt->execute($fields)) {
//		  debug::add('PDO'.$text.' Rows affected',$stmt->rowCount());
            return $stmt;
        } else {
            $error = $stmt->errorInfo();
            echo 'ERROR INFO:' . var_dump($error);
//			debug::add('PDO ERROR',$error[2]);
//			debug::add('PDO SQL',$sql);
            //echo 'PDO ERROR'.$error[2];
            return false;
        }
    }

    private function debugSql($sql, $parms) {
        return 'true';
    }

    static function loadInstance() {
        if (!isset(self::$instance)) {
            $db = settings::getSettings('system', 'DB');
            try {
                $db_dsn = settings::getSettings($db, 'DB_DSN');
                $user = settings::getSettings($db, 'DB_USER');
                $passwd = settings::getSettings($db, 'DB_PASSWD');
                self::$instance = new \PDO($db_dsn, $user, $passwd);
                self::$instance->setAttribute(\PDO::ATTR_DEFAULT_FETCH_MODE, \PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                log::logError('PDO Connection error ' . $e->getMessage());
                die();
            }
            return self::$instance;
        } else {
            //          debug::add('PDO Info', 'Instance pre loaded');
            log::logInfo('PDO Info', 'Instance pre loaded');
        }
    }

}

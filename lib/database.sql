
CREATE TABLE `promotions` (
  `code` mediumint(9) NOT NULL,
  `name` varchar(80) NOT NULL,
  `timedate_sent` datetime DEFAULT NULL,
  `recipients` mediumint(9) DEFAULT NULL,
  `opened` mediumint(9) DEFAULT NULL,
  `open_rate` float DEFAULT NULL,
  `delivery_attempts` mediumint(9) DEFAULT NULL,
  `complaints` mediumint(9) DEFAULT NULL,
  `unsubscribed` mediumint(9) DEFAULT NULL,
  `last_opened` datetime DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `times_opened` mediumint(9) DEFAULT NULL,
  `total_clicks` mediumint(9) DEFAULT NULL,
  `last_clicked` datetime DEFAULT NULL,
  `clickthrough_rate` float DEFAULT NULL,
  `clicked` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `emails` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `code` mediumint(9) NOT NULL,
  `name` varchar(80) NOT NULL,
  `tracker_id` varchar(80) NOT NULL,
  `opened` varchar(1) NOT NULL,
  `times_opened` smallint(6) NOT NULL,
  `timedate_sent` datetime DEFAULT NULL,
  `timedate_opened` datetime DEFAULT NULL,
  `email` varchar(80) NOT NULL,
  `times_clicked` smallint(6) DEFAULT NULL,
  `last_clicked` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=875 DEFAULT CHARSET=latin1;

CREATE TABLE `recipients` (
  `id` bigint(10) NOT NULL AUTO_INCREMENT,
  `firstname` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `email` varchar(150) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1033 DEFAULT CHARSET=latin1;

# INSERT
INSERT INTO `ses_mailer`.`recipients` (`id`, `firstname`, `surname`, `email`) VALUES ('1', 'David', 'Brent', 'dbrent@wernhamhogg.com');
INSERT INTO `ses_mailer`.`recipients` (`id`, `firstname`, `surname`, `email`) VALUES ('2', 'Alan', 'Partiridge', 'apartridge@peartree.com');
INSERT INTO `ses_mailer`.`recipients` (`id`, `firstname`, `surname`, `email`) VALUES ('3', 'Lucien', 'Sanchez', 'lsanchez@darkplace.com');
INSERT INTO `ses_mailer`.`recipients` (`id`, `firstname`, `surname`, `email`) VALUES ('4', 'Maurizio', 'Giambrone', 'm_giambrone@hotmail.co.uk');
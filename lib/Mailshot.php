<?php

namespace SesMailer\Lib;

use Aws\Common\Credentials\Credentials;
use Aws\Ses\SesClient;
use Guzzle\Service\Exception\CommandTransferException;

/**
 * Description of mailshot
 *
 * @author mgiambro
 */
class Mailshot {

    private $sesClient = null;
    private $emailTemplate;
    private $mode;
    private $subject;
    private $promotionCode;

    const TEMPLATE_FILEPATH = "../templates/";
    const LOG_USER = "MGiambro MAILSHOT";
    const SOURCE_EMAIL = "MGiambro <maurizio@clevertech.tv>";
    const CMD_BATCH_SIZE = 4;   // The size of the batch of commands to be put on to the SES queue.
    const RUN_BATCH_SIZE = 10; // The number of mails you're sending for the current run.

    function __construct($setup = true) {

        if ($setup == true) {
            $credentials = new Credentials(settings::getSettings('amazons3', 'accesskey'), settings::getSettings('amazons3', 'Secretkey'));
            $this->sesClient = SesClient::factory(array(
                        'credentials' => $credentials,
                        'region' => 'eu-west-1'));
        }
    }

    public function processBatch() {

        $emails = $this->getEmailArray();

        $size = sizeof($emails);
        // Create promotion record
        if ($this->hasPromotionRecord()) {
            $this->updatePromotionRecord($size);
        } else {
            $this->createPromotionRecord($size);
        }

        //   echo var_dump($emails);
        $emailBatch = new SplQueue();
        $emailBatch->setIteratorMode(SplQueue::IT_MODE_KEEP);

        $emailCount = sizeof($emails);
        $logStr = 'Adding: ' . $emailCount . " " . "recipients to the Mailout queue ..\n";
        log::logInfo($logStr, Mailshot::LOG_USER);
        echo $logStr;
        // Get number of batches
        $numberOfBatches = $this->CalculateNoOfCmdBatches($emailCount);
        $logStr = 'Number of command batches: ' . $numberOfBatches . "\n";
        log::logInfo($logStr, Mailshot::LOG_USER);

        // Generate SendEmail commands to batch
        $currentBatchSize = 0;
        $batchNumber = 1;
        foreach ($emails as $email) {
            // Create tracking record
            $trackerId = $this->generateTrackingId();
            $trdetail = array('trackerid' => $trackerId, 'custref' => $email['custref'], 'email_encrypted' => $email['email_encrypted'], 'md5' => $email['md5']);
            //     echo var_dump($trdetail);
            $this->createTrackingRecord($trdetail);
            $emailCommand = $this->sesClient->getCommand('SendEmail',
                    // GENERATE COMMAND PARAMS FROM THE $email DATA
                    $this->constructEmail($email, $trackerId)
            );
            $emailBatch->enqueue($emailCommand);
            $currentBatchSize++;
            if ($currentBatchSize >= Mailshot::CMD_BATCH_SIZE ||
                    ($batchNumber == $numberOfBatches & $currentBatchSize == $size)) {
                //echo "BATCH NUMBER: " . $batchNumber . "\n";
                echo "Sending command batch..." . $batchNumber . "\n";

                //### SEND ###
                $this->sendBatch($emailBatch);

                if ($batchNumber == $numberOfBatches) {
                    echo "FINISHED MAILING RUN BATCH\n";
                }
                $batchNumber++;
                // Reset the batch
                unset($emailBatch);
                $emailBatch = new SplQueue();
                $emailBatch->setIteratorMode(SplQueue::IT_MODE_KEEP);
                // Reset batch counter
                $currentBatchSize = 0;
            }
        }

    }

    public function CalculateNoOfCmdBatches($totalRecipients) {
        if (Mailshot::CMD_BATCH_SIZE > 0 && $totalRecipients > 0) {
            $div = $totalRecipients / Mailshot::CMD_BATCH_SIZE;
            return Mailshot::round_up($div, 1);
        } else {
            return 0;
        }
    }

    public function hasPromotionRecord() {
        $sql = 'SELECT code from promotions WHERE code = :code';
        $params = array('code' => $this->promotionCode);
        $dao = new Dao();
        $stmt = $dao->query($sql, $params);
        if ($stmt) {
            $results = array();
            while ($entry = $stmt->fetch()) {
                $results[] = $entry['code'];
            }
            return sizeof($results);
        } else {
            return false;
        }
    }

    public function setPromotionCode($promotionCode) {
        $this->promotionCode = $promotionCode;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
        $logStr = "Setting the " . Mailshot::LOG_USER . " subject to '" . $subject . "'\n\n";
        log::logInfo($logStr, Mailshot::LOG_USER);
        echo $logStr;
    }

    public function setMode($mode) {
        $this->mode = $mode;
        $logStr = "\nSetting " . Mailshot::LOG_USER . " to " . $mode . " mode.\n";
        log::logInfo($logStr, Mailshot::LOG_USER);
        echo $logStr;
    }

    public function setEmailTemplate($templateFilename) {
        $this->emailTemplate = file_get_contents(Mailshot::TEMPLATE_FILEPATH . $templateFilename);
        $logStr = 'Set email template: ' . $templateFilename . " " . "succsessfully\n";
        log::logInfo($logStr, Mailshot::LOG_USER);
        echo $logStr;
    }

    public function templateAvailable($templateFilename) {
        return file_exists(Mailshot::TEMPLATE_FILEPATH . $templateFilename);
    }

    private function constructEmail($emailArgs, $trackerId) {
        $email = array(
            // Source is required
            'Source' => Mailshot::SOURCE_EMAIL,
            // Destination is required
            'Destination' => array(
                'ToAddresses' => array($emailArgs['email'])
            ),
            // Message is required
            'Message' => array(
                // Subject is required
                'Subject' => array(
                    // Data is required
                    'Data' => $this->subject,
                    'Charset' => 'UTF-8',
                ),
                // Body is required
                'Body' => array(
                    'Text' => array(
                        // Data is required
                        'Data' => 'ClicThat Events Promotion 2015',
                        'Charset' => 'UTF-8',
                    ),
                    'Html' => array(
                        // Data is required
                        'Data' => $this->personaliseTemplate($emailArgs['name'], $emailArgs['email'], $emailArgs['md5'], $trackerId),
                        'Charset' => 'UTF-8',
                    ),
                ),
            ),
            'ReplyToAddresses' => array(Mailshot::SOURCE_EMAIL),
            'ReturnPath' => Mailshot::SOURCE_EMAIL
        );
        return $email;
    }

    private function buildEmailArrayTestEncrypted() {
        // Get all recipients
        $recipients = array(
            // me
            array('firstnames' => 'WXYrVDRGVVJYYTE1bmxuNUg3TGQ5Zz09', 'email' => 'bytNSkVRVnlKY241ZjNLVjk4K0c4SnAwTGNkYXlBSGFXY2pka0ZpeXErcz0=', 'md5' => '7fbeea1ddbb503c35fa6074cbbb694de',
                'ref' => '-144', 'email_encrypted' => 'bytNSkVRVnlKY241ZjNLVjk4K0c4SnAwTGNkYXlBSGFXY2pka0ZpeXErcz0=')
        );

        $return = array();

        // Create email entry for each recipients
        foreach ($recipients as $line) {
            $md5 = $line['md5'];
            $key = $this->porticor->getCombinedKey($md5);
            $name = crypt::decrypt($line['firstnames'], $key);
            $email = crypt::decrypt($line['email'], $key);
            $recipient = array('name' => $name, 'email' => $email, 'md5' => $line['md5'], 'custref' => $line['ref'], 'email_encrypted' => $line['email']);
            array_push($return, $recipient);
        }

        //    echo var_dump($recipients);
        return $return;
    }

    private function buildEmailArrayTestSmall() {
        $recipients = array(
            array('name' => 'Maurizio', 'email' => 'maurizio@clevertech.tv', 'md5' => '8db81b5a6aaa6d449b265154ea66288f',
                'custref' => '144', 'email_encrypted' => 'L1A4eHdJZjB1cDUxWUo3bGdqSk5wVEV3OTh6MFpudkFTYm54ZEpyT21TYz0=')
        );
        return $recipients;
    }

    private function buildEmailArrayTest() {

        $limit = 10;

        $recipients = array();

        for ($i = 0; $i <= $limit; $i++) {
            $recipient = array('name' => 'bod' . $i, 'email' => 'bod' . $i . '@clevertech.solutions', 'md5' => '8db81b5a6aaa6d449b265154ea66288f' . $i,
                'custref' => $i, 'email_encrypted' => 'L1A4eHdJZjB1cDUxWUo3bGdqSk5wVEV3OTh6MFpudkFTYm54ZEpyT21TYz0=' . $i);
            array_push($recipients, $recipient);
        }

        return $recipients;
    }

    private function buildEmailArray() {
        // Get all recipients
        $sent = $this->getRefsSentForPromotion($this->promotionCode);
        $recipients = $this->getRecipients($this->promotionCode, $sent);
        //      echo sizeof($customers) . "\n";
        //      echo var_dump($customers);

        $return = array();

        // Create email entry for each recipient
        foreach ($recipients as $line) {
            $name = $line['firstnames'];
            $email = $line['email'];
            $recipient = array('name' => $name, 'email' => $email);
            array_push($return, $recipient);
        }

        //    echo var_dump($recipients);
        return $recipients;
    }

    private function createTrackingRecord($trdetail) {
        $fields = array(
            'code' => $this->promotionCode,
            'name' => $this->subject,
            'tracker_id' => $trdetail['trackerid'],
            'opened' => 'n',
            'times_opened' => '0',
            'timedate_sent' => date('Y-m-d H:i:s'),
            'times_clicked' => '0'
        );
        dbpdologin::dbInsert('promotional_emails', $fields);
    }

    private function createPromotionRecord($recipients) {

        $fields = array(
            'code' => $this->promotionCode,
            'name' => $this->subject,
            'timedate_sent' => date('Y-m-d H:i:s'),
            'recipients' => $recipients
        );
        echo print_r($fields);
        $dao = new Dao();
        $dao->dbInsert('promotions', $fields);
    }

    private function updatePromotionRecord($recipients) {
        $currentRecipients = $this->getNumberOfRecipients();
        $newRecipients = $currentRecipients + $recipients;

        $fields = array(
            'recipients' => $newRecipients
        );
        
        $dao = new Dao();
        $dao->dbUpdate('promotions', $this->promotionCode, $fields, 'code');
    }

    private function getNumberOfRecipients() {
        $sql = 'SELECT recipients from promotions WHERE code = :code LIMIT 1';
        $params = array('code' => $this->promotionCode);
        $stmt = dbpdologin::query($sql, $params);
        if ($stmt) {
            $result = $stmt->fetch();
            return $result['recipients'];
        } else {
            return false;
        }
    }

    private function getEmailArray() {
        $emails = null;
        if (strtolower($this->mode) == 'test') {
            $emails = $this->buildEmailArrayTestSmall();
        } else if (strtolower($this->mode) == 'live') {
            $emails = $this->buildEmailArray();
        }
        return $emails;
    }

    private function sendBatch($emailBatch) {
        try {
            // Send the batch
            // echo var_dump(iterator_to_array($emailBatch));
            echo "Executing batch send .. \n";
            $successfulCommands = $this->sesClient->execute($emailBatch);
        } catch (CommandTransferException $e) {
            $successfulCommands = $e->getSuccessfulCommands();
            // Requeue failed commands
            foreach ($e->getFailedCommands() as $failedCommand) {
                $logStr = "FAILED SEND MESSAGE ID: " . $failedCommand->getResult()->get('MessageId') . "\n";
                log::logInfo($logStr, Mailshot::LOG_USER);
                echo $logStr;
                $emailBatch->enqueue($failedCommand);
            }
        }

        $sentCount = 0;
        foreach ($successfulCommands as $command) {
            $sentCount++;
            echo 'Sent message: ' . $command->getResult()->get('MessageId') . "\n";
            echo $command->getPath("subject");
        }
        $emailBatch->setIteratorMode(SplQueue::IT_MODE_DELETE);
        echo $sentCount . " " . "emails sent successfully.\n";
    }

    private function generateTrackingId() {
        return uniqid(md5(time()), true);
    }

    private function personaliseTemplate($name, $email, $md5, $trackerId) {
        $rawTemplate = $this->getEmailTemplate();
        $templateSubName = $this->replaceTemplateValue('##name##', $name, $rawTemplate);
        $templateSubId = $this->replaceTemplateValue('##tracking_hash##', $trackerId, $templateSubName);
        $template = $this->replaceTemplateValue('####unsubscribe####', $this->createUnsubscribeLink($md5, $email), $templateSubId);
        return $template;
    }

    private static function createUnsubscribeLink($customerMd5, $customerEmail) {

        $salt = settings::getSettings('unsubscribe', 'salt');
        $hash = hash('md5', $customerMd5 . $salt);
        $data = openssl_encrypt($customerEmail . '|' . $hash, "AES-256-CBC", '', 0, substr($customerMd5, 0, 16));
        $sign = unsubscribe::sslSign($data);
        $link = '';

        if ($sign['result']) {
            $preparedHash = urlencode($data);
            $preparedSig = urlencode(base64_encode($sign['signature']));

            $url = settings::getSettings('unsubscribe', 'siteurl');
            $link = $url . 'unsubscribe.php?email=' . $customerEmail
                    . '&hash=' . $preparedHash
                    . '&signature=' . $preparedSig;
        } else {
            log::logError('Error signing Unsubscribe email link.');
        }
        return $link;
    }

    private function replaceTemplateValue($from, $to, $template) {
        return str_ireplace($from, $to, $template);
    }

    public function getAllRecipients() {
        $sql = 'SELECT firstname , surname, ref, email FROM recipients WHERE marketing <> :out';
        $params = array('out' => 'Out');
        $stmt = dbpdo::query($sql, $params);
        if ($stmt) {
            $results = array();
            while ($entry = $stmt->fetch()) {
                $results[] = $entry;
            }
            return $results;
        } else {
            return false;
        }
    }

    /*
     * Reuturns only the recipients who have not already been sent a mail
     * for supplied promotion code.
     */

    public function getRecipients($sent) {

        if (sizeOf($sent) > 0) {
            $refsAsString = implode(',', $sent);
            //      echo $refsAsString;
            $sql = "SELECT firstnames, surname, id, email FROM recipients WHERE id NOT IN (" . $refsAsString . ") LIMIT " . Mailshot::RUN_BATCH_SIZE;
            //     echo "REMAINING\n";
        } else {
            $sql = 'SELECT firstname, surname, id, email FROM recipients LIMIT ' . Mailshot::RUN_BATCH_SIZE;
            //     echo "ALL\n";
        }
        $dao = new Dao();
        $params = array('out' => 'Out');
        //  $stmt = dbpdo::query($sql, $params);
        $stmt = $dao->query($sql);
        if ($stmt) {
            $results = array();
            while ($entry = $stmt->fetch()) {
                $results[] = $entry;
            }
            //      echo sizeOf($results) . "\n";
            return $results;
        } else {
            return false;
        }
    }

    public function getRefsSentForPromotion($promotionCode) {
        $sql = 'SELECT email from emails WHERE code = :code';
        $params = array('code' => $promotionCode);

        $dao = new Dao();

        $stmt = $dao->query($sql, $params);
        if ($stmt) {
            $results = array();
            while ($entry = $stmt->fetch()) {
                $results[] = $entry['email'];
            }
            return $results;
        } else {
            return false;
        }
    }

    private function getEmailTemplate() {
        return $this->emailTemplate;
    }

    static function round_up($number, $precision = 2) {
        $fig = (int) str_pad('1', $precision, '0');
        return (ceil($number * $fig) / $fig);
    }

    static function round_down($number, $precision = 2) {
        $fig = (int) str_pad('1', $precision, '0');
        return (floor($number * $fig) / $fig);
    }

}

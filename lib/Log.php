<?php

namespace SesMailer\Lib;

class log {

    static function logg($type, $data, $user = '', $notify = false, $filePrefix = '') {
        $fname = $filePrefix . date('d-m-y') . '.log';
        $path = __DIR__ . '/logs/' . $fname;
  //      debug::add('log path', $path);

        $fh = fopen($path, 'a');

        $now = date('d-m-y H:i:s');

        $logLine = $now . ' | ';
        $logLine.=str_pad($type, 8, ' ', STR_PAD_RIGHT) . ' | ';
        $logLine.=str_pad('{' . $user . '}', 8, ' ', STR_PAD_RIGHT);
        $logLine.=' | ' . self::arrayCheck($data);
        $logLine.='  {' . $_SERVER['PHP_SELF'] . '}';
        $logLine.=PHP_EOL;
        fwrite($fh, $logLine);
        fclose($fh);
        if ($notify) {
            self::sendPushover($logLine);
        }
    }

    static function arrayCheck($data) {
        if (is_array($data)) {
            $op = 'Array : ';
            foreach ($data as $key => $value) {
                $op.=$key . '=>' . $value . ',';
            }
            return $op;
        } else {
            return 'String : ' . $data;
        }
    }

    static function logErrorPlus($data, $user = '', $notify = false, $filePrefix = '') {
        $bt = debug_backtrace();
        $btsub = self::getSubArray($bt, 2);
        $trace = self::getSubArray($btsub, 'class') . ' / ' . self::getSubArray($btsub, 'function') . ' / ' . self::getSubArray($btsub, 'line');


        self::logg('PHP_ERROR', $trace . ' :::' . $data, $user, $notify, $filePrefix = '');
    }

    static function logError($data, $user = '', $notify = false, $filePrefix = '') {
        self::logg('ERROR', $data, $user, $notify, $filePrefix = '');
    }

    static function logWarning($data, $user = '', $notify = false, $filePrefix = '') {
        self::logg('WARNING', $data, $user, $notify, $filePrefix = '');
    }

    static function logInfo($data, $user = '', $notify = false, $filePrefix = '') {
        self::logg('INFO', $data, $user, $notify, $filePrefix);
    }

    static function logDebug($data, $user = '', $notify = false, $filePrefix = '') {
        self::logg('DEBUG', $data, $user, $notify, $filePrefix = '');
    }

    static function logKeyStep($data, $user = '', $notify = false, $filePrefix = '') {
        self::logg('KEYSTEP', $data, $user, $notify, $filePrefix = '');
    }

    static function startErrorLogs() {
        set_error_handler(array('log', 'logPHPError'), E_ALL);
    }

    static function logPHPError($errno, $errstr, $errfile, $errline) {

        self::logErrorPlus($errno . ':' . $errstr . '...' . $errfile . ': (' . $errline . ') ', '', true);
    }

    private static function getSubArray($inArray, $target) {
        $retVal = '';
        if (is_array($inArray)) {
            if (array_key_exists($target, $inArray)) {

                $retVal = $inArray[$target];
            }
        }

        return $retVal;
    }

}

?>
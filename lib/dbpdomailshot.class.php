<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dbpdomailshot
 *
 * @author mgiambro
 */
class dbpdomailshot extends dbpdo {

    private static $instance;

    static function loadInstance() {
        if (!isset(self::$instance)) {
            debug::add('PDO Info', 'Loading Instance');
            $host = settings::getSettings('database-admin', 'hostname');
            $dbname = settings::getSettings('database-admin', 'dbname');
            debug::add('db name', $dbname);
            try {
                self::$instance = new PDO("mysql:host=$host;dbname=$dbname", settings::getSettings('database-admin', 'username'), settings::getSettings('database-admin', 'password'));
                self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                debug::add('PDO Connection error', $e->getMessage());
                //           echo debug::show();
                die();
            }
            return self::$instance;
        } else {
            debug::add('PDO Info', 'Instance pre loaded');
        }
    }

    public static function query($sql, $parms = array(), $validate = true) {
        self::loadInstance();
        self::debugSql($sql, $parms);
        if ($validate) {
            if (!self::validateSQL($sql, $parms)) {
                return false;
            }
        }
        $stmt = self::$instance->prepare($sql);
        return self::runStatement($sql, $stmt, $parms, 'Prepared completed');
    }

    public static function dbUpdateTracker($fields, $opened) {
        self::loadInstance();
        if ($opened) {
            $sql = 'UPDATE promotional_emails SET times_opened = times_opened + 1 WHERE (tracker_id = :tracker_id)';
        } else {
            $sql = 'UPDATE promotional_emails SET times_opened = 1, opened = \'y\', timedate_opened = :timedate_opened WHERE (tracker_id = :tracker_id)';
        }

        $stmt = self::$instance->prepare($sql);
        self::runStatement($sql, $stmt, $fields, "Update Completed");
    }

    public static function dbUpdateClicked($fields) {
        self::loadInstance();

        $sql = 'UPDATE promotional_emails SET times_clicked = times_clicked + 1, last_clicked = :last_clicked WHERE (tracker_id = :tracker_id)';
        $stmt = self::$instance->prepare($sql);
        self::runStatement($sql, $stmt, $fields, "Update Completed");
    }

    private static function runStatement($sql, $stmt, $fields, $text) {
        if ($stmt->execute($fields)) {
            debug::add('PDO' . $text . ' Rows affected', $stmt->rowCount());
            return $stmt;
        } else {
            $error = $stmt->errorInfo();
            echo var_dump($error);
            debug::add('PDO ERROR', $error[2]);
            debug::add('PDO SQL', $sql);
            //echo 'PDO ERROR'.$error[2];
            return false;
        }
    }

    private static function debugSql($sql, $parms) {
        $retVal = $sql;
        if (sizeof($parms) > 0) {
            foreach ($parms as $key => $value) {
                $retVal = str_ireplace($key, "'" . $value . "'", $retVal);
            }
        }

        debug::add('SQL Debug', $retVal);
    }

    private static function validateSQL($sql, $parm) {
        if (strrpos($sql, '"' > 0)) {
            debug::add('PDO ERROR', 'Must use prepared statements 1');
            return false;
        }
        if (strrpos($sql, "'" > 0)) {
            debug::add('PDO ERROR', 'Must use prepared statements 2');
            return false;
        }
        //now split on the wehere statement and check the rest
        $where = explode('where', strtolower($sql));

        if (array_key_exists(1, $where)) {
            $toCheck = $where[1];
        } else {
            //$toCheck=$where[0];
            $toCheck = '';
        }
        $eqs = explode('=', $toCheck);
        $first = array_shift($eqs);
        $parmCount = 0;
        $valueList = '';
        foreach ($eqs as $value) {
            $valueList.=$value . ',';
            //debug::add($value);
            //debug::add(substr($value,0,1));
            if (substr(trim($value), 0, 1) != ':') {
                debug::add('PDO ERROR', 'Must use prepared statements 3');
                return false;
            }
            $parmCount++;
        }
        debug::add('Value List ', $valueList);
        if (stripos($sql, "insert into" === 0)) {
            if ($parmCount != sizeof($parm)) {
                debug::add('PDO ERROR', 'Parameter count mismatch ' . sizeof($parm) . ' passed, expecting ' . $parmCount);
                return false;
            }
        }
        return true;
    }

}

<?php

class unsubscribe {
    
    private static $feedback_table = 'feedback';
    private static $unsubscribe_table = 'unsubscribe';
    
    public static function sendNotificationEmail($to = false, $countFeedback = false) {
        if($to) {
            /*
                $headers = "From: noreply@clicthat.tv\r\n";
                $headers .= "Reply-To: support@clicthat.tv\r\n";
                $headers .= "MIME-Version: 1.0\r\n";
                $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            * 
            */

            $subject = 'New Feedback Available';

            $totalFeedback = ($countFeedback) ? '<p>Total number of feedback : '.$countFeedback . '</p>' : '';
            $message = '<html>'
                        . '<body>'
                            . '<h1>New Feedback Available.</h1>'
                            . '<p>Please login to DIMOS to approve new feedback.</p>'
                            . $totalFeedback
                        . '</body>'
                    . '</html>';
            
            //$res = mail($to, $subject, $message, $headers);
            emailer::sendemail($subject,$message,$to,'ClicThat');
        }
        return false;
    }

    public static function getNewFeedback() {
        $sql = '
            SELECT
                ref, dealmd5, rating, date, name, email, description
            FROM '.self::$feedback_table.'
        ';
        $params = array();
        $stmt = dbpdounsubscribe::query($sql, $params);
        if ($stmt) {
            $feedback = array();
            while ($entry = $stmt->fetch()) {
                $feedback[] = $entry;
            }
            return $feedback;
        } else {
            debug::add('ERROR', 'No new feedback available.');
            return false;
        }
    }
    
    public static function deleteFeedbackByRef($ref = false) {
        if($ref) {
            dbpdounsubscribe::dbDelete(self::$feedback_table, $ref);
        }
    }
    
    public static function deleteUnsubscribeByRef($ref = false) {
        if($ref) {
            dbpdounsubscribe::dbDelete(self::$unsubscribe_table, $ref);
        }
    }
    
    public static function createUnsubscribeLink($customerMd5, $customerEmail) {
        
        $salt = settings::getSettings('unsubscribe','salt');
        $hash = hash('md5', $customerMd5 . $salt);
        $data = openssl_encrypt($customerEmail . '|'.  $hash, "AES-256-CBC", '', 0, substr($customerMd5, 0, 16));
        $sign = self::sslSign($data);
        
        if($sign['result']) {
            $preparedHash = urlencode($data);
            $preparedSig = urlencode(base64_encode($sign['signature']));

            $url = settings::getSettings('unsubscribe','siteurl');
            $link = '<a style="color:#303030;" href="'.$url.'unsubscribe.php?email='.$customerEmail
                        .'&hash='.$preparedHash
                        .'&signature='.$preparedSig
                    .'" target="_blank">unsubscribe</a>';
            $res = '<tr>'
                        . '<td>'
                            . 'Please '.$link.' if you are no longer interested in receiving marketing emails from ClicThat.'
                        .'</td>'
                    .'</tr>';
        } else {
            log::logError('Error signing Unsubscribe email link.');
            $res = '';
        }
        return $res;
    }
    
    public static function getNewUnsubscribes() {
        $sql = '
            SELECT
                ref, email, date, hash, sig
            FROM '.self::$unsubscribe_table.'
        ';
        $params = array();
        $stmt = dbpdounsubscribe::query($sql, $params);
        if ($stmt) {
            $unsubscribes = array();
            while ($entry = $stmt->fetch()) {
                $unsubscribes[] = $entry;
            }
            return $unsubscribes;
        } else {
            debug::add('ERROR', 'No new unsubscribes available.');
            return false;
        }
    }
    
    public static function sslVerify($data, $signature) {
        
        $key = file_get_contents( settings::getSettings('unsubscribe','publickey') );
        $public_key = openssl_get_publickey($key);

        // Verify
        $res = self::verifyData($data, $signature, $public_key, OPENSSL_ALGO_SHA1);
        openssl_free_key($public_key);
        
        return $res;
    }
    
    private static function verifyData($data, $signature, $public_key_res, $signature_alg) {
       return openssl_verify($data, $signature, $public_key_res, $signature_alg);
    }
    
    public static function sslSign($data) {
        
        $key = file_get_contents( settings::getSettings('unsubscribe','privatekey') );
        $private_key = openssl_get_privatekey($key);
        $signature = '';

        // Sign
        $res = self::signData($data, $signature, $private_key, OPENSSL_ALGO_SHA1);
        openssl_free_key($private_key);
        
        return array(
            'signature' => $signature, 
            'result'=> $res
        );
    }
    
    private static function signData($data, &$signature, $private_key, $signature_alg) {
        return openssl_sign($data, $signature, $private_key);
    }
    

}

?>
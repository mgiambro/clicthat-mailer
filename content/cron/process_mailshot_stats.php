<?php

require_once '/var/ct/lib/aws/aws.phar';

require_once '/var/ct/lib/mailshot/Campaign.class.php';

$stats = new Statistics();

$controller = new CampaignController();

$campaigns = $controller->getAllCampaigns();

foreach ($campaigns as $campaign) {

    $campaign_code = $campaign['campaign_code'];

    $date_sent = $stats->getDateSent($campaign_code);

    $stats->updateCampaignTable($date_sent, $campaign_code);

}







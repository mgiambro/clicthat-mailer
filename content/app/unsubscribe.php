<?php
include ('sections/_top.php');

$email = clean::get('email');
$data = clean::get('hash');
$signature = base64_decode(clean::get('signature'));
?>
<div class="deal-confirm1">
    <div class="inner30">
        <h2>Marketing Email Unsubscribe</h2>
        <?php
        //functions::dump($data);
        //functions::dump($signature);
        
        if (clean::post('unsubscribe') == "yes") {
            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $fields = array(
                    'email' => $email,
                    'hash' => base64_encode($data),
                    'sig' => base64_encode($signature),
                    'date' => date('Y-m-d H:i:s', time()),
                );
                $res = functions::insertDbRow($fields, 'unsubscribe');
                if ($res) {
                    echo '<div class="message">'
                            . '<p>You have now been unsubscribed and will no longer receive any marketing emails from ClicThat.</p>'
                            . '<p>If you wish to re-subscribe in the future you can do so within your &quot;My Profile&quot; area within ClicThat.</p>'
                        . '</div>';
                } else {
                    echo '<div id="unsubscribe-validation">We are currently dealing with your request. Please <a href="http://www.clicthat.com/main/contact-us.html" title="Contact Us" target="_blank">Contact Us</a> for further information.</div>';
                }
            } else {
                echo '<div id="unsubscribe-validation">Invalid Unsubscribe Link.</div>';
            }
        } else {
            if (clean::post('unsubscribe') == 'no')
                echo '<div id = "unsubscribe-validation">Please choose &quot;Yes&quot; if you wish to unsubscribe.</div>';
                $verify = unsubscribe::sslVerify($data, $signature);
                if ($verify == 1) { // Verified..
                $alreadyUnsubscribed = unsubscribe::checkUserUnsubscribe($email);
                if (!$alreadyUnsubscribed) {
                    ?>
                    <form name="unsubscribe" id="unsubscribe-form" action="" method="post">
                        <div class="element">
                            <label for="name">Email address</label>
                            <input type="text" class="disabled" disabled="disabled" value="<?php echo $email; ?>" name="" />
                        </div>
                        <div class="element">
                            <label for="name">Are you sure you want to unsubscribe</label>
                            <input type="radio" value="yes" name="unsubscribe" /> Yes
                            &nbsp;
                            <input type="radio" value="no" name="unsubscribe" checked="checked" /> No
                        </div>
                        <input class="btn-acc-item" type="submit" value="Confirm Unsubscribe">
                    </form>
                    <?php
                } else {
                    echo '<div id="unsubscribe-validation">You are already unsubscribed. Please <a href="http://www.clicthat.com/main/contact-us.html" title="Contact Us" target="_blank">Contact Us</a> for further information.</div>';
                }
            } else {
                echo '<div id="unsubscribe-validation">Invalid Unsubscribe Link.</div>';
            }
        }
        ?>
    </div>
</div>

<?php
include ('sections/footer.php');

<?php
require '../lib/prepend.php'; 

$tracker_id = clean::get('id');

if($tracker_id != null){
    
    $opened = opened($tracker_id);

    if($opened){
        $fields = array(
            'tracker_id' => $tracker_id);
    } else {
        $fields = array(
            'tracker_id' => $tracker_id,
            'timedate_opened' => date('Y-m-d H:i:s'));
    }

     dbpdomailshot::dbUpdateTracker($fields, $opened);
 
} else {
    // Log something .. 
    echo "No tracking ID";
}

function opened($tracker_id){
    $sql = "SELECT opened from promotional_emails WHERE tracker_id = :tracker_id;";
    $params = array('tracker_id' => $tracker_id);

 $stmt = dbpdomailshot::query($sql, $params);

    if($stmt){
        $opened = $stmt->fetch()['opened'];
        if ($opened=='y'){
            return true;
        }
    }
    return false;
}
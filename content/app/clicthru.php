<?php

require '../lib/prepend.php';

$page = clean::get('page');
$tracker_id = clean::get('id');
$baseURL = 'http://www.clicthat.com/main/';
$appendURL = '';
// echo $page;
//echo $tracker_id;

switch ($page) {
    case 'treat_week':
        $appendURL = 'treatweek.html';
        break;
    case 'current_sales':
        $appendURL = 'category-current.html';
        break;
    case 'future_sales':
        $appendURL = 'category-future.html';
        break;
    case 'office_sales':
        $appendURL = 'category-both.html';
        break;
    case 'past_sales':
        $appendURL = 'pastsales.html';
        break;
    default:
        $appendURL = 'treatweek.html';
        break;
}

// Updated times clicked for tracker id
if ($tracker_id != null) {
    $fields = array(
        'tracker_id' => $tracker_id,
        'last_clicked' => date('Y-m-d H:i:s'));
    dbpdomailshot::dbUpdateClicked($fields);  
} else {
    // Log something .. 
    echo "No tracking ID";
}

$opened = opened($tracker_id);

if(!$opened){
     $fields = array(
            'tracker_id' => $tracker_id,
            'timedate_opened' => date('Y-m-d H:i:s'));
     dbpdomailshot::dbUpdateTracker($fields, $opened);
}

// Redirect to clicthat page
$redirectTo = $baseURL . $appendURL;
header('Location:' . $redirectTo);

function opened($tracker_id){
    $sql = "SELECT opened from promotional_emails WHERE tracker_id = :tracker_id;";
    $params = array('tracker_id' => $tracker_id);
    $stmt = dbpdomailshot::query($sql, $params);
    
    if($stmt){
        $opened = $stmt->fetch()['opened'];
        if ($opened=='y'){
            return true;
        }
    }
    return false;
}
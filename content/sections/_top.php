<?php
	//show db name when not in live
	$dbname='';
	$db=settings::getSettings('database','dbname');
	$dbarr=explode('.',$db);
	if($dbarr[0]!='bagthatlive'){
		$dbname=' - '.$dbarr[0];
	}

?>
<!DOCTYPE html>
<html lang="en"> 
<head>
	<meta charset="utf-8">
		
		<title>ClicThat - Mailer<?php echo $dbname;?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
		<link rel="shortcut icon" href="../images/rb.gif" type="image/x-icon"/>
		
		<link rel="stylesheet" type="text/css" href="../style/bootstrap.min.css" media="screen" />
		

		<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

		<link rel="stylesheet" type="text/css" href="../style/cal.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="../style/dropzone.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="../style/slidebars.min.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="../style/main.css" media="screen" />
		<link rel="stylesheet" type="text/css" href="../style/font-awesome.min.css" media="screen" />

		<script type="text/javascript" src="../js/jquery182.js"></script>

		<script type="text/javascript" src="../js/wysihtml5-0.3.0_rc2.min.js"></script>
		<script type="text/javascript" src="//code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
		<script type="text/javascript" src="../js/timepicker.js"></script>
		<script type="text/javascript" src="../js/qr.js"></script>
		<script type="text/javascript" src="../js/bootstrap.min.js"></script>
		<script type="text/javascript" src="../js/crafty_postcode.class.js"></script>
		<script type="text/javascript" src="../js/postcode.js"></script>
		<script type="text/javascript" src="../js/dropzone.js"></script>
		<script type="text/javascript" src="../js/onload.js"></script>


<script type="text/javascript" src="../js/medium.min.js"></script>
<link rel="stylesheet" type="text/css" href="../style/medium.min.css" media="screen" />
<link rel="stylesheet" type="text/css" href="../style/themes/bootstrap.min.css" media="screen" />

	<link rel="stylesheet" type="text/css" href="../paste/paste.css" media="screen" />
	<script type="text/javascript" src="../paste/paste.js"></script>

	</head>
	<body>	

		
		
<?php
if(!isset($nomenu)) {
$nomenu = false;
}
	if($nomenu){
	}else{
		echo "<div class='padfix'></div>";
		include '../s/_topmenu.php';
	}
?>

	<div class='main userform'>
		

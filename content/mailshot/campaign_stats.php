<?php
include '../sections/_top.php';
require '../../lib/aws/aws.phar';
require_once '../../lib/mailshot/Campaign.class.php';
require_once '../../lib/functions.class.php';
require_once '../../lib/paging.class.php';
require_once '../../lib/porticorendpoint.class.php';
require_once '../../lib/crypt.class.php';
?>

<?php
$stats = new Statistics();
$controller = new CampaignController();
$campaign_code = clean::get('code');
$campaign = $controller->getCampaign($campaign_code);
$delivered = new DateTime($campaign->timedate_sent);

// List
$list = clean::get('list');

switch ($list) {
    case "opened":
        $listResults = $controller->getRecipientOpens($campaign_code, 'y');
        break;
    case "clicked":
         $listResults = $controller->getRecipientClicked($campaign_code);
        break;
    case "ignored":
        $listResults = $controller->getRecipientOpens($campaign_code, 'n');
        break;
    case "unsubscribed":
        $listResults = $controller->getRecipientUnsubscribes($delivered, $campaign_code);
        break;
    default:
        $listResults = $controller->getRecipientOpens($campaign_code, 'y');
}

echo '<div style="float:left;width:420px;">';
echo "<h2 style='color:#666666'>Mailshot Statistics</h2>";
echo '</div>';
echo "<br/>";

echo '<div style="float:left;width:420px;">';
echo "<h4><span style='color:#CC3333'>" . number_format($campaign->recipients) . "</span> Recipients</h4>";
echo '</div>';

echo "<br/><br/><br/>";
echo '<div style="float:left;width:420px;">';
echo "<span style='font-weight:bold; color:#666666'>Subject: </span>" . $campaign->campaign_name;
echo '</div>';

echo '<div style="float:left;width:320px;">';
echo "<span style='font-weight:bold; color:#666666'>Delivered: </span>" . $delivered->format("F j, Y, g:i a");
echo '</div>';
echo "<br/><br/>";
?>

<?php echo "<span style='font-weight:bold; color:#666666'>Open Rate: </span>&nbsp<span style='color:#32A467; font-weight:bold;'>" . $campaign->open_rate . '%</span>' .
        "&nbsp(Industry Avg: " . Statistics::OPEN_RATE_AVG . "%)<br/><br/>"; ?>
<div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $campaign->open_rate; ?>"
         aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $campaign->open_rate; ?>%; font-weight:bold;">
        Open rate <?php echo $campaign->open_rate; ?>%
    </div>
</div>

<?php echo "<span style='font-weight:bold; color:#666666'>Clickthru Rate: </span>&nbsp<span style='color:#32A467; font-weight:bold;'>" . $campaign->clickthrough_rate . '%</span>' .
        "&nbsp(Industry Avg: " . Statistics::CLICK_THRU_AVG . "%)<br/><br/>"; ?> 
<div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="<?php echo $campaign->clickthrough_rate; ?>"
         aria-valuemin="0" aria-valuemax="100" style="width:<?php echo $campaign->clickthrough_rate; ?>%; font-weight:bold;">
        Click through rate <?php echo $campaign->clickthrough_rate; ?>%
    </div>
</div>

<?php
echo '<table class="table table-bordered table-striped"><thead>';
echo functions::tableTitles(
        array(
            "<span style='font-weight:bold; color:#006666'>Opened</span>",
            "<span style='font-weight:bold; color:#006666''>Times Opened</span>",
            "<span style='font-weight:bold; color:#006666'>Clicked</span>",
            "<span style='font-weight:bold; color:#006666''>Total Clicks</span>",
            "<span style='font-weight:bold; color:#006666''>Bounced</span>",
            "<span style='font-weight:bold; color:#006666''>Unsubscribed</span>"
        )
);
echo '</thead>';
echo functions::tableRow(
        array(
            number_format($campaign->opened),
            number_format($campaign->times_opened),
            number_format($campaign->clicked),
            number_format($campaign->total_clicks),
            number_format($campaign->bounced),
            number_format($campaign->unsubscribed)
        )
);
echo '</table>';

echo '<div style="float:left;width:420px;">';
echo "<span style='font-weight:bold; color:#666666'>Delivery Attempts : </span>" . number_format($campaign->delivery_attempts);
echo '</div>';

echo '<div style="float:left;width:320px;">';
echo "<span style='font-weight:bold; color:#666666'>Last Opened: </span>" . format_date($campaign->last_opened);
echo '</div>';
echo "<br/>";
echo '<div style="float:left;width:420px;">';
echo "<span style='font-weight:bold; color:#666666'>Marked as Spam by Recipient: </span>" . number_format($campaign->complaints);
echo '</div>';

echo '<div style="float:left;width:420px;">';
echo "<span style='font-weight:bold; color:#666666'>Last Clicked: &nbsp;</span>" . format_date($campaign->last_clicked);
echo '</div>';

echo "<br/>";
echo '<div style="float:left;width:420px;">';
echo "<span style='font-weight:bold; color:#666666'>Rejected: </span>" . number_format($campaign->rejected);
echo '</div>';

echo '<div style="float:left;width:420px;">';
echo "<span style='font-weight:bold; color:#A0522D'>Stats Last Updated: &nbsp;" . format_date($campaign->last_updated) . "</span>";
echo '</div>';
echo "<br/><br/>";
?>
<div id='pagenav' class='subnav'>
    <ul id="scrollspy" class='nav nav-pills'>
        <li <?php echo (clean::get('list') == 'opened' || clean::get('list') == '') ? 'class="active"' : ''; ?>><a href= <?php echo 'campaign_stats.php?code=' . $campaign_code . '&list=opened' ?>>Opened</a></li>
    <!--
        <li 
            <?php echo (clean::get('list') == 'ignored') ? 'class="active"' : ''; ?>
        ><a href= <?php echo 'campaign_stats.php?code=' . $campaign_code . '&list=ignored' ?>>Didn&#8217;t Open</a>
        </li>
    -->
        <li <?php echo (clean::get('list') == 'clicked') ? 'class="active"' : ''; ?>><a href= <?php echo 'campaign_stats.php?code=' . $campaign_code . '&list=clicked' ?>>Clicked</a></li>
        <li <?php echo (clean::get('list') == 'unsubscribed') ? 'class="active"' : ''; ?>><a href= <?php echo 'campaign_stats.php?code=' . $campaign_code . '&list=unsubscribed' ?>>Unsubscribed</a></li>
    </ul>
</div>
<br/>
<div id='recipients'>
<?php buildTable($listResults); ?>
</div>

<?php
paging::showPageToolsAdmin(false);
include '../sections/_footer.php';
?>

<?php

function buildTable($results) {
    $porticor = new porticorendpoint(settings::getSettings('porticor', 'current_master_key_name'));
    if ($results && sizeOf($results) > 0) {
        $table = null;
        $table .= '<table class="table table-bordered table-striped"><thead>';
        $table .= functions::tableTitles(
                        array(
                            'Ref',
                            'Email',
                            'Times Opened',
                            'Last Opened',
                            'Times Clicked',
                            'Last Clicked'
                        )
        );
        $table .= '</thead>';


        foreach ($results as $result) {
            $md5 = $result['md5'];
            $key = $porticor->getCombinedKey($md5);
            $email = crypt::decrypt($result['email'], $key);
            if ($email != null && $email != '') {
                $table .= functions::tableRow(
                                array(
                                    $result['custref'],
                                    crypt::decrypt($result['email'], $key),
                                    $result['times_opened'],
                                    format_date($result['timedate_opened']),
                                    $result['times_clicked'],
                                    format_date($result['last_clicked'])
                                )
                );
            }
        }

        $table .= '</table>';
        echo $table;
    } else {
        echo "No results found";
    }
}

function format_date($date) {
    $ret_val = $date;
    if($date != null){
       $ret_val = date_format(date_create($date),"F j, Y, g:i a");
    }
    return $ret_val;
}

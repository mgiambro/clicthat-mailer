<?php 
include '../sections/_top.php';
require_once '../../lib/mailshot/Campaign.class.php';
require_once '../../lib/functions.class.php';
?>

<h1>Mailshot Campaigns</h1>

<?php
$controller = new CampaignController();
       
$campaigns = $controller->getAllCampaigns();
//print("<pre>".print_r($controller->getAllCampaigns(),true)."</pre>");

echo '<table class="table table-bordered table-striped"><thead>';
echo functions::tableTitles(
        array(
            'Code',
            'Name',
            'Date Sent'
        )
);
echo '</thead>';
if ($campaigns) {
    foreach ($campaigns as $line) {
        echo functions::tableRow(
                array(
                    $line['campaign_code'],
                    '<a href="campaign_stats.php?code=' . $line ['campaign_code'] . '">' . $line ['campaign_name'] . '</a>',
                    $line ['timedate_sent']
                )
        );
    }
} else {
    echo '<tr><td colspan="6" style="text-align:center";>No campaigns found</td></tr>';
}
echo '</table>';
?>

<?php include '../sections/_footer.php' ?>


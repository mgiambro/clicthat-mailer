<?php
require_once  '../../../vendor/autoload.php';
//namespace SesMailer\Content;

use \SesMailer\Lib\Mailshot;

set_time_limit(0);
ini_set('memory_limit', '256M');
parse_str(implode('&', array_slice($argv, 1)), $_GET);

// Do some checks .. 
$templateFilename = $_GET['template'];
$mode = $_GET['mode'];
$subject = $_GET['subject'];
$code = $_GET['code'];

/*
  $memory_limit = ini_get('memory_limit');
  echo "MEMORY LIMIT: " . $memory_limit . "\n";
 */

// Help menu
if (sizeof($argv) == 2 && strtolower($argv[1]) == 'help') {
    echo "\nREQUIRED PARAMETERS(options are delimited by :)\n\nmode=[test:live]\ntemplate=[name_of_file.html]\nsubject=['email subject'] // MUST BE IN QUOTES\ncode=[campaign code]\n";
    exit;
}

$mailShot = new Mailshot();

// If the mode parameter is passed and is 'Test', run in test mode.
if ($mode == null) {
    echo "\nIn order to run the mailshot you must enter a mode value.\n\nOptions are:\n\nmode=test\nmode=live\n";
    exit;
} else {
    $mailShot->setMode($mode);
}

// Template exists?
if ($mailShot->templateAvailable($templateFilename)) {
    $mailShot->setEmailTemplate($templateFilename);
} else {
    echo "\nEmail template: '" . $templateFilename . "' " . "could not be located.\nPlease ensure it is in the /var/ct/emailtemplate directory";
    exit;
}

// Test email subject is entered
if ($subject == null) {
    echo "\nYou must enter a subject for mailshot e.g.\nsubject=ClicThat January Promotion\n";
    exit;
} else {
    $mailShot->setSubject($subject);
}

// Test Campaign Code is entered
if ($code == null) {
    echo "\nYou must enter a Campaign Code for mailshot e.g.\ncode=1001\n";
    exit;
} else {
    $mailShot->setPromotionCode($code);
}

if ($mode == "live") {
// Recipients
    $sent = $mailShot->getRefsSentForPromotion($code);
    //echo 'SENT' . var_dump($sent);  
    $recipients = $mailShot->getRecipients($sent);
    //echo 'RECIPIENTS:'. var_dump($recipients);
    $numberOfRecipients = sizeOf($recipients);
    $numberOfBatches = $mailShot->CalculateNoOfCmdBatches($numberOfRecipients);

    echo "RUN BATCH SIZE: " . Mailshot::RUN_BATCH_SIZE . "\n";
    echo "COMMAND BATCH SIZE: " . Mailshot::CMD_BATCH_SIZE . "\n";
    echo "No OF COMMAND BATCHES FOR RUN: " . $numberOfBatches . "\n";
    echo "RECIPIENTS (no more than " . Mailshot::RUN_BATCH_SIZE . ", can be less): " . $numberOfRecipients . "\n\n";

    if ($recipients === false) {
        echo "\nThe database query has errored.\nPlease check the database settings.";
        exit;
    }

    if ($numberOfRecipients === 0) {
        echo "\nAll recipients have been mailed for Campaign: " . $code;
        exit;
    }
}
// All checks passed. Are you sure? ..
echo "\nAre you sure you want to do run the '" . $subject . "' mailshot?  Type 'yes' to continue: ";
$handle = fopen("php://stdin", "r");
$line = fgets($handle);
if (trim($line) != 'yes') {
    echo "ABORTING!\n";
    exit;
}
echo "\n";
echo "Continuing...\n";

// Start the mailshot
$mailShot->processBatch();

